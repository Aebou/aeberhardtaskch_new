
<?php require_once('inc/config.php');?>
<?php require_once('inc/security.php'); ?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foundation for Sites</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  </head>
  <body>

<?php require_once('template/header.php'); ?>

<style media="screen">
  .about{
    padding: 50px;
  }
</style>


  <div class="about">
    <h1>Mon projet</h1>
    <p>Ce projet a pour but de créer une liste de taches. Nous avons utilisé plusieurs technologies pour réaliser ce travail</p>
    <p>Cette liste de tâche devait comporter plusieurs fonctionnalité telle que : </p>
    <p>-Ajouter une tache</p>
    <p>-Supprimer une tache</p>
    <p>-Modifier une tache</p>
    <p>-Ajouter un utilisateur</p>
    <p>-Supprimer un utilisateur</p>
    <p>etc.</p>
    <p>

    </p>

    <p>html, CSS, Foundation, php etc.</p>
    <p>Afin de garder une sauvergarde de notre projet, nous avons utilisé bitbucket pour faire des pushs de notre site internet</p>
    <p>Ce projet fut intéressant et a permis de revoir les bases ainsi qu'apprendre de nouvelles connaissances en développment web</p>

  </div>


    <?php require_once('template/footer.php'); ?>




  </body>
</html>
