-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `thetask`;
CREATE TABLE `thetask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `create_at` date NOT NULL,
  `create_by` int(11) NOT NULL,
  `due` date NOT NULL,
  `asigned_to` int(11) DEFAULT NULL,
  `priorite` enum('1','2','3','4','5','6','7','8','9','10') NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `done_by` int(11) DEFAULT NULL,
  `done_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `create_by` (`create_by`),
  KEY `asigned_to` (`asigned_to`),
  KEY `done_by` (`done_by`),
  CONSTRAINT `thetask_ibfk_1` FOREIGN KEY (`create_by`) REFERENCES `user` (`id`),
  CONSTRAINT `thetask_ibfk_2` FOREIGN KEY (`asigned_to`) REFERENCES `user` (`id`),
  CONSTRAINT `thetask_ibfk_3` FOREIGN KEY (`done_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `thetask` (`id`, `description`, `create_at`, `create_by`, `due`, `asigned_to`, `priorite`, `status`, `done_by`, `done_at`) VALUES
(58,	'test1',	'2017-06-25',	1,	'2017-06-25',	1,	'7',	'0',	NULL,	NULL),
(59,	'test2',	'2017-06-20',	10,	'2017-06-23',	1,	'10',	'0',	NULL,	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2017-06-25 17:01:33
