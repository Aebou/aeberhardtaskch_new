<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="style.css" />
  <title>CV Cédric Aeberhard</title>
</head>

<body>

  <div class="traitJaune"></div>
  <div id="divLeft">
    <header>

      <section>
        <div class="carreNom">
          <p class="leNom">Cédric Aeberhard</p>
          <p class="CV"> Curriculum vitae</p>
        </div>
      </section>
    </header>

    <section>
      <div class="photo">
        <img src="assets/img/photo.jpg" alt="photo de profil" width="210" />
      </div>
    </section>

    <section>
      <div class="mesInfos">
        <h2 class="profilmini">Profil</h2>
        <h4>Nom</h4><p>Cédric Aeberhard</p>
        <h4>Date de naissance</h4><p>28 aooût 1993</p>
        <h4>Statut</h4><p>Célibataire</p>
        <h4>Adresse</h4><p>Chemin des coires 2<br>1772 Ponthaux</p>
        <h4>Réseau</h4>

        <p><a href="https://www.facebook.com/cedric.aeberhard.9">Facebook</a></p>
        <p><a href="https://twitter.com/Aeb0u">Twitter</a></p>
        <p><a href="https://www.instagram.com/aeb0u/?hl=fr">Instagram</a></p>




      </div>



    </section>
  </div>

  <div id="divRight">
    <section class="formationPrincipal">
      <div class="formation">
        <h1>Formation</h1>
        <p>
          <ul>
            <li>2009-2013 Apprentissage en informatique à Ref-flex</li>
            <li>2009-2013 Maturité professionnel Ref-flex</li>
            <li>Août 2014 - Août 2015 Service civil à la Résidence St-Martin de Cottens</li>
            <li>2015-2019 HEIA Fribourg en internet et communication</li>
          </ul>
        </p>
      </div>
    </section>

    <section>
      <div class="langue">
        <h1>Langues</h1>
        <p>

          <table>
            <tr>
              <td>Français</td>
              <td><meter value="10" min="0" max="10">2 out of 10</meter></td>
            </tr>
            <tr>
              <td>Allemand</td>
              <td><meter value="4" min="0" max="10">2 out of 10</meter></td>
            </tr>
            <tr>
              <td>Anglais</td>
              <td><meter value="7" min="0" max="10">2 out of 10</meter></td>
            </tr>
          </table>

    </section>

        <section>
          <div class="CompetProf">
            <h1>Compétences professionnelles</h1>
            <p>
              <Table>
                <tr>
                <td>JAVA</td>
              <td><meter value="7" min="0" max="10">2 out of 10</meter></td>

                </tr>

                <tr>
                <td>HTML</td>
              <td><meter value="5" min="0" max="10">2 out of 10</meter></td>

                </tr>

                <tr>
                <td>CSS</td>
              <td><meter value="5" min="0" max="10">2 out of 10</meter></td>

                </tr>

                <tr>
                <td>Office</td>
              <td><meter value="10" min="0" max="10">2 out of 10</meter></td>

                </tr>

                <tr>
                <td>Photoshop</td>
              <td><meter value="7" min="0" max="10">2 out of 10</meter></td>

                </tr>

                <tr>
                <td>Lightroom</td>
              <td><meter value="10" min="0" max="10">2 out of 10</meter></td>

                </tr>
              </Table>
            </p>
          </div>
        </section>

        <section>
          <div class="CompetencePerso">
            <h1>Compétences personnelles</h1>
            <p>
              <ul>
                <li>Relations humaines</li>
                <li>Responsable</li>
                <li>Ponctuel</li>
                <li>Travaux de groupe</li>
                <li>Flexible</li>
              </ul>
            </p>
          </div>
        </section>

        <section>
          <div class="loisirs">
            <h1>Loisirs</h1>
            <p>
              <ul>
                <li>Hockey</li>
                <li>Fitness</li>
                <li>Photos</li>
                <li>Hearthstone</li>
              </ul>
            </p>
          </div>
        </section>



      </div>



      <div class="bas"> </div>








    </body>





    </html>
