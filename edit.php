
  <?php

  require_once('inc/config.php');
  require_once('inc/security.php');

  $redirect = "insert.php";
  $description ="";
  $create_at="";
  $due="";
  $priorite="";
  $task="";
  $assigned_to="";
  $ismodify=false;

  if(isset($_REQUEST['task'])){
    $sth = $db->prepare("SELECT * FROM thetask WHERE id=?");
    $sth->execute(array($_REQUEST['task']));
    $data=$sth->fetchAll();

    $redirect = "update.php";

    $task = $_REQUEST['task'];
    $description = $data[0]["description"];
    $create_at=$data[0]["create_at"];
    $due=$data[0]["due"];
    $priorite=$data[0]["priorite"];
    $assigned_to=$data[0]["asigned_to"];
    $ismodify=true;

  }


  ?>

  <!doctype html>
  <html class="no-js" lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Foundation for Sites</title>
      <link rel="stylesheet" href="css/app.css">
      <link rel="stylesheet" href="assets/css/font-awesome.css">
          <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    </head>

   <body>
       <?php require_once('template/header.php'); ?>
    <div class="container">

      <main>
<form id="edit" name="edit" method="post" action= "<?php echo $redirect;?>">
  <input type="hidden" name="task" value = "<?php echo $task;?>">
  <table class="row tableajout">
    <tr>
      <td>Description</td>
      <td><label>
        <input name="description" type="text" id="description" value = "<?php echo $description;?>" />
      </label></td>
    </tr>
    <?php if(!$ismodify) : ?>
        <tr>
      <td>Date</td>
      <td><label>
        <input name="create_at" type="date" id="create_at" value = "<?php echo $create_at;?>" />
      </label></td>
      <?php else: ?>
        <input name="create_at" type="hidden" id="create_at" value = "<?php echo $create_at;?>" />
      <?php endif; ?>
    </tr>

    <tr>
      <td>assigned_to</td>
      <td><label>
        <?php
        $stmt = $db->prepare("select * from user");
        $stmt->execute();
        $user= $stmt->fetchAll();
        ?>


        <select class="form-edit-input" name="assigned_to"  id="assigned_to">
          <?php foreach ($user as $row): ?>

            <?php if(isset($assigned_to)) : ?>
              <option <?php echo $assigned_to==$row['id']?'selected':'';?> value="<?php echo $row['id'];?>" > <?php echo $row['name'];?> </option>
            <?php else: ?>
              <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?> </option>
            <?php endif; ?>

          <?php endForeach; ?>
        </select>
          </label></td>
    </tr>









    <?php if(!$ismodify) : ?>
      <td>Due</td>
      <td><label>
        <input name="due" type="date" id="due" value = "<?php echo $due;?>" />
      </label></td>
    </tr>
  <?php else: ?>
    <input name="create_at" type="hidden" id="create_at" value = "<?php echo $create_at;?>" />
  <?php endif; ?>

      <?php if(!$ismodify) : ?>
  <?php else: ?>
    <input name="create_at" type="hidden" id="create_at" value = "<?php echo $create_at;?>" />
  <?php endif; ?>
    <tr>
      <td>Priorité(1-10)</td>
      <td><label>
        <input name="priorite" type="int" id="priorite" value = "<?php echo $priorite;?>" />
      </label></td>
    </tr>
    <tr>
      <td colspan="2"><label>
        <input name="Valider" type="submit" id="valider" value="valider" />
      </label></td>
    </tr>
  </table>





                </main>

               <?php require_once('template/footer.php'); ?>

              </div>




            </body>
            </html>
