$(document).foundation();

$('[data-delete]').click(function(e){
  var id = $(this).data('delete');
  var line = $(this);
  $.ajax({
    url: "delete.php?task=" + id,
  }).done(function(data) {
    line.parents(".tasklist-item").fadeOut();
  //  alert("test");
  });

});

$('[data-delete-user]').click(function(e){
  var id = $(this).data('delete-user');
  var line = $(this);
  $.ajax({
    url: "deleteuser.php?task=" + id,
  }).done(function(data) {
    line.parents(".tasklist-item-user").fadeOut();
  //  alert("test");
  });

});
