

<?php require_once('inc/config.php');?>
<?php require_once('inc/security.php'); ?>


<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foundation for Sites</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  </head>
  <body>

<?php require_once('template/header.php'); ?>



    <div class="row"
        <ul>
          <li class="tasklist-item-principal">
            <span class="tasklist-item-id">IDs</span>
            <span class="tasklist-item-description">Description</span>
          <span class="tasklist-item-date">Date</span>
            <span class="tasklist-item-create_by">Create_by</span>
          <span class="tasklist-item-due">Due</span>
            <span class="tasklist-item-assigned_to">Assigned_to</span>
            <span class="tasklist-item-done_by">Done_by</span>
          <span class="tasklist-item-priorite">Priorite</span>
             <span class="tasklist-item-status"></span>




          </li>
          <?php foreach ($datadone as $row) : ?>
        <li class="tasklist-item <?php if($row['status'] == 1) {echo "tracer";}?>">
          <a class="fait" href="edit.php?task=<?php echo $row['id'];?>">
          <span class="tasklist-item-id"><?php echo $row['id']?></span>
          <span class="tasklist-item-description"><?php echo $row['description']?></span>
          <span class="tasklist-item-date"><?php echo $row['create_at']?></span>
          <span class="tasklist-item-create_by"><?php echo $row['author']?></span>
          <span class="tasklist-item-due"><?php echo $row['due']?></span>
          <span class="tasklist-item-assigned_to"><?php echo $row['assignee_name']?></span>
          <span class="tasklist-item-done_by"><?php echo $row['executer_name']?></span>
          <span class="tasklist-item-priorite"><?php echo $row['priorite']?></span>

          </a>


          <span class="tasklist-item-bouton"><a class="poubelle" href="#" data-delete="<?php echo $row['id'];?>"><i class="fa fa-trash" aria-hidden="true"></i></a></span><a class="fait" href="done.php?task=<?php echo $row['id'];?>"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></span></span>

        </li>
        <?php endForeach;?>
        </ul>
    </div>



    <a class="add" href="edit.php"><i class="fa fa-plus-circle fa-4x" aria-hidden="true"></i></a>

    <?php require_once('template/footer.php'); ?>




  </body>
</html>
