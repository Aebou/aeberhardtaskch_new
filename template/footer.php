<footer class="row footer expanded">
 <h2>
	<small>&copy; Copyright HEIA, Cédric Aeberhard</small>
 </h2>

 <script src="bower_components/jquery/dist/jquery.js"></script>
 <script src="bower_components/what-input/dist/what-input.js"></script>
 <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
 <script src="js/app.js"></script>
 
</footer>
