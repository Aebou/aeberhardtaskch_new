
<?php require_once('inc/config.php');?>
<?php require_once('inc/security.php'); ?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foundation for Sites</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  </head>
  <body>

<?php require_once('template/header.php'); ?>



    <div class="row"
        <ul>
          <li class="tasklist-item-principal-user">
            <span class="tasklist-item-id">IDs</span>
            <span class="tasklist-item-name">Name</span>
            <span class="tasklist-item-email">Email</span>
             <span class="tasklist-item-status"></span>




          </li>
          <?php foreach ($datau as $row) : ?>
        <li class="tasklist-item-user">
          <span class="tasklist-item-id"><?php echo $row['id']?></span>
          <span class="tasklist-item-name"><?php echo $row['name']?></span>
          <span class="tasklist-item-email"><?php echo $row['email']?></span>
          <span class="tasklist-item-bouton-user"><a class="poubelle" href="#" data-delete-user="<?php echo $row['id'];?>"><i class="fa fa-trash" aria-hidden="true"></i></a></span>

        </li>
        <?php endForeach;?>
        </ul>
    </div>



    <a class="add" href="edituser.php"><i class="fa fa-plus-circle fa-4x" aria-hidden="true"></i></a>

    <?php require_once('template/footer.php'); ?>




  </body>
</html>
